module.exports = {
  apps : [{
    name: 'NodeCluster',
    script: 'server.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '750M',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'ubuntu',
      key  : 'credentials/test_shadow.pem',
      host : '13.235.165.98',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:hemantgoyal009/node-cluster.git',
      path : '/home/ubuntu/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
