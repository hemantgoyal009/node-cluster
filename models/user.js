const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: String,
    email: { type: String, required: true }
}, { timestamps: true, collection: "User" });

const User = module.exports = mongoose.model("User", UserSchema);