const User = require("../models/user");

const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
    User.find({}).then(users => {
        res.status(200).json(users);
    }).catch(error => {
        res.status(500).send(error);
    });
}); 

router.post("/new", (req, res) => {
    const userObj = req.body;
    User.create(userObj).then(user => {
        res.status(200).json(user);
    }).catch(error => {
        res.status(500).send(error);
    })
});

module.exports = router;