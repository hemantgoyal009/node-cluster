const cluster = require("cluster");
const express = require("express");
const isPrime = require("./isPrime");
const mongoose = require("mongoose");
const cors = require("cors");

const MONGO_URI = `mongodb://node_cluster:12345@localhost:27017/node_cluster`;
const MONGO_OPTS = { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false };

const userRoutes = require("./routes/user");

mongoose.connect(MONGO_URI, MONGO_OPTS).then(() => {
    console.log("Successfully connected to MongoDB");
}).catch(error => {
    console.log("Couldn't connect to database");
    throw error;
});


if (cluster.isMaster) {
    const numCpu = require("os").cpus().length;
    for (let i = 0; i < numCpu; i++) {
        cluster.fork();
    }
} else {
    const app = express();

    app.use(cors());
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());

    app.get("/", (req, res) => {
        const primes = [];
        const max = Number(req.query.max) || 100;

        for (let i = 1; i <= max; i++) {
            if (isPrime(i)) primes.push(i);
        }

        res.status(200).json(primes);

    });

    app.use("/users", userRoutes);

    app.listen(process.env.PORT || 3030, () => {
        console.log("Server listening on port: 3030");
    });
}

cluster.on("exit", (worker) => {
    console.log('Worker died: ' + worker.id);
    cluster.fork();
});