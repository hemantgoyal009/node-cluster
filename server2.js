const express = require("express");
const isPrime = require("./isPrime");

const app = express();

app.get("/", (req, res) => {
    const primes = [];
    const max = Number(req.query.max) || 100;

    for (let i = 1; i <= max; i++) {
        if (isPrime(i)) primes.push(i);
    }

    res.status(200).json(primes);

});

app.listen(process.env.PORT || 3030, () => {
    console.log("Server listening on port: 3030");
});